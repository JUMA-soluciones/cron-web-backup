#/bin/bash

# Bash script to easily backup folders and database from local or remote
# FTP or SFTP locations to a bucket in AWS S3.
#
# Copyright 2021 MarMarAba <mario@marmaraba.com>
#
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#######################################
# GLOBAL DEFINITIONS
#######################################

trap "exit ${EXIT_STATUS}" TERM
trap ctrl_c INT
export TOP_PID=$$



#######################################
# What to do when traping CTRL+C.
#
# Shows a message and exits the
# application with 255 status
# using the exit_app() function.
#
#######################################

function ctrl_c() {
  echo
  log_to_warn "${FUNCNAME[0]}(): Received CTRL+C, exiting..."
  exit_app 255 clean
}


#######################################
# Exit the aplication with order.
#
# Delete the temporal folder, if needed
# and logs the exit cause.
#
# PARAMETERS:
#   exit_status {1}
#   clean_tmp_folder {2}
#
#######################################

function exit_app()
{
  log_to_debug "${FUNCNAME[0]}():"
  EXIT_STATUS=0

  [[ -n ${1} ]] && { EXIT_STATUS=${1} ; }
  [[ -n ${2} ]] && { delete_temp_folder ; }


  if [[ ${LOG_PROFILE} == 1 ]]
  then
    local end_time=`date +%s`
    local run_time=$((end_time-START_TIME))
    log_to_info "PROFILING: '${BACKUP_NAME}' backup finished in ${run_time} seconds" force
  fi

  log_to_debug "${FUNCNAME[0]}(): Exiting application with status code ${EXIT_STATUS}"

#  kill -s TERM $TOP_PID

  exit ${EXIT_STATUS}
}


#######################################
# Set the defaults for global
# variables, load the MAIN_CONF_FILE
# variables if the file exists, load
# CONFIG_FILE variables if the option
# is passed in command line arguments,
# parse command line options and
# update the appropriate global
# variables, and check if the mandatory
# variables are set.
#
# VARIABLES:
#   Almost all...
# RETURN:
#   0 if operations succeeds, non-zero
#   on error.
#######################################

function load_configuration() {
  # Set defaults
  LOG_LEVEL=info
  SYS_TMP_DIR=/tmp
  MAIN_CONF_FILE="/etc/bjackup/main.conf"
  LOG_PROFILE=0
  MAINTAIN_TMP_DATA=0

  TMP_DIR_TEMPLATE="bjackup-job-"

  # Load main.conf file if exists
  [[ -e "${MAIN_CONF_FILE}" ]] && { source ${MAIN_CONF_FILE} ; }

  # Save args
  ARGS=( "$@" )

  while [[ $# -gt 0 ]]
  do
    key="$1"
    case $key in
        -h|--help)
        usage complete
        return 1
        ;;
        -c|--config-file)
        [[ -z "${2}" ]] && return 1
        local config_file=${2}
        [[ ! -e ${config_file} ]] &&
          { log_to_error "${FUNCNAME[0]}(): Unable to locate config file at ${config_file}. Use absolute path."; return 1; }
        shift 2
        ;;
        *)    # other options
        shift 1
        ;;
    esac
  done

  # Load config file options
  [[ -e "${config_file}" ]] && { source ${config_file} ; }

  # Restore args
  set -- "${ARGS[@]}"

  while [[ $# -gt 0 ]]
  do
    key="$1"

    case $key in
        -h|--help) # noop
          shift 1
        ;;
        -c|--config-file) # noop
          shift 2
        ;;
        -p|--purge-tmp-dirs)
          purge_temp_folders ;
          exit_app 0 ;
        ;;
        -l|--log-level)
          [[ -z "${2}" ]] && return 1
          LOG_LEVEL=${2}
          shift 2
        ;;
        -m|--maintain-tmp-data)
          MAINTAIN_TMP_DATA=1
          shift 1
        ;;
        -s|--script-run)
          SCRIPT_RUN=1
          shift 1
        ;;
        -v)
          LOG_LEVEL=WARN
          shift 1
        ;;
        -vv)
          LOG_LEVEL=INFO
          shift 1
        ;;
        -vvv)
          LOG_LEVEL=DEBUG
          shift 1
        ;;
        -t|--time)
          LOG_PROFILE=1
          shift 1
        ;;
        --backup-folders)
          [[ -z "${2}" ]] && return 1
          IFS=',' read -ra BACKUP_FOLDERS <<< "${2}"
          shift 2
        ;;
        --storage-protocol)
          [[ -z "${2}" ]] && return 1
          STORAGE_PROTOCOL=${2,,}
          shift 2
        ;;
        --storage-host)
          [[ -z "${2}" ]] && return 1
          STORAGE_HOST=${2}
          shift 2
        ;;
        --storage-port)
          [[ -z "${2}" ]] && return 1
          STORAGE_PORT=${2}
          shift 2
        ;;
        --storage-user)
          [[ -z "${2}" ]] && return 1
          STORAGE_USER=${2}
          shift 2
        ;;
        --storage-pass)
          [[ -z "${2}" ]] && return 1
          STORAGE_PASS=${2}
          shift 2
        ;;
        --db-connection)
          [[ -z "${2}" ]] && return 1
          DB_CONNECTION=${2}
          shift 2
        ;;
        --db-protocol)
          [[ -z "${2}" ]] && return 1
          DB_PROTOCOL=${2}
          shift 2
        ;;
        --db-host)
          [[ -z "${2}" ]] && return 1
          DB_HOST=${2}
          shift 2
        ;;
        --db-port)
          [[ -z "${2}" ]] && return 1
          DB_PORT=${2}
          shift 2
        ;;
        --db-user)
          [[ -z "${2}" ]] && return 1
          DB_USER=${2}
          shift 2
        ;;
        --db-pass)
          [[ -z "${2}" ]] && return 1
          DB_PASS=${2}
          shift 2
        ;;
        --db-name)
          [[ -z "${2}" ]] && return 1
          DB_NAME=${2}
          shift 2
        ;;
        --ssh-tunnel-host)
          [[ -z "${2}" ]] && return 1
          SSH_TUNNEL_HOST=${2}
          shift 2
        ;;
        --ssh-tunnel-port)
          [[ -z "${2}" ]] && return 1
          SSH_TUNNEL_PORT=${2}
          shift 2
        ;;
        --ssh-tunnel-user)
          [[ -z "${2}" ]] && return 1
          SSH_TUNNEL_USER=${2}
          shift 2
        ;;
        --ssh-tunnel-pass)
          [[ -z "${2}" ]] && return 1
          SSH_TUNNEL_PASS=${2}
          shift 2
        ;;
        --pre-script-cmd)
          [[ -z "${2}" ]] && return 1
          PRE_SCRIPT_CMD=${2}
          shift 2
        ;;
        --post-script-cmd)
          [[ -z "${2}" ]] && return 1
          POST_SCRIPT_CMD=${2}
          shift 2
        ;;
        --scripting-connection)
          [[ -z "${2}" ]] && return 1
          SCRIPTING_CONNECTION=${2}
          shift 2
        ;;
        --scripting-host)
          [[ -z "${2}" ]] && return 1
          SCRIPTING_HOST=${2}
          shift 2
        ;;
        --scripting-port)
          [[ -z "${2}" ]] && return 1
          SCRIPTING_PORT=${2}
          shift 2
        ;;
        --scripting-user)
          [[ -z "${2}" ]] && return 1
          SCRIPTING_USER=${2}
          shift 2
        ;;
        --scripting-pass)
          [[ -z "${2}" ]] && return 1
          SCRIPTING_PASS=${2}
          shift 2
        ;;
        --aws-connection-profile)
          [[ -z "${2}" ]] && return 1
          AWS_CONNECTION_PROFILE=${2}
          shift 2
        ;;
        --aws-region)
          [[ -z "${2}" ]] && return 1
          AWS_REGION=${2}
          shift 2
        ;;
        --aws-bucket)
          [[ -z "${2}" ]] && return 1
          AWS_BUCKET=${2}
          shift 2
        ;;
        --sys-tmp-dir)
          [[ -z "${2}" ]] && return 1
          SYS_TMP_DIR=${2}
          shift 2
        ;;
        *)    # unknown option
        echo
        echo ERROR: ${1}: Opción no reconocida.
        return 1
        ;;
    esac
  done

  if  [[ ${LOG_LEVEL,,} != fatal ]] &&
      [[ ${LOG_LEVEL,,} != error ]] &&
      [[ ${LOG_LEVEL,,} != warn ]] &&
      [[ ${LOG_LEVEL,,} != info ]] &&
      [[ ${LOG_LEVEL,,} != debug ]] &&
      [[ -n ${LOG_LEVEL} ]]
  then
    log_to_warn "\"${LOG_LEVEL}\" is not valid log level. Defaulting to \"debug\""
    LOG_LEVEL=debug
  fi

  {
    { [[ -z "${DB_PROTOCOL}" ]] && [[ "${DB_CONNECTION}" != "disabled" ]] && log_to_warn "${FUNCNAME[0]}(): The config parameter \${DB_PROTOCOL} is mandatory"; } ||
    { [[ -z "${STORAGE_PROTOCOL}" ]] && log_to_warn "${FUNCNAME[0]}(): The config parameter \${STORAGE_PROTOCOL} is mandatory"; } ||
    { [[ -z "${BACKUP_NAME}" ]] && log_to_warn "${FUNCNAME[0]}(): The config parameter \${BACKUP_NAME} is mandatory"; } ||
    false 
  } &&
  { log_to_error "${FUNCNAME[0]}(): Check your configuration." force && usage && return 1; }

  return 0
}


#######################################
# Create a file in TMP_DIR with the
# backup used config.
#
# VARIABLES:
#   Almost all...
#   TMP_DIR (mandatory)
# RETURN:
#   0
#######################################

save_current_config_to_tmp_dir(){
  log_to_debug "${FUNCNAME[0]}():"

  echo "## Configuration file for $0" >> ${TMP_DIR}/used_config.conf ;
  echo "" >> ${TMP_DIR}/used_config.conf ;
  echo "# Created on $(date -Ins)" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${BACKUP_NAME} ]] && echo "BACKUP_NAME=${BACKUP_NAME}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${BACKUP_FOLDERS} ]] && echo "BACKUP_FOLDERS=${BACKUP_FOLDERS}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${STORAGE_PROTOCOL} ]] && echo "STORAGE_PROTOCOL=${STORAGE_PROTOCOL}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${STORAGE_HOST} ]] && echo "STORAGE_HOST=${STORAGE_HOST}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${STORAGE_PORT} ]] && echo "STORAGE_PORT=${STORAGE_PORT}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${STORAGE_USER} ]] && echo "STORAGE_USER=${STORAGE_USER}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${STORAGE_PASS} ]] && echo "STORAGE_PASS=${STORAGE_PASS}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_CONNECTION} ]] && echo "DB_CONNECTION=${DB_CONNECTION}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_PROTOCOL} ]] && echo "DB_PROTOCOL=${DB_PROTOCOL}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_HOST} ]] && echo "DB_HOST=${DB_HOST}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_PORT} ]] && echo "DB_PORT=${DB_PORT}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_USER} ]] && echo "DB_USER=${DB_USER}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_PASS} ]] && echo "DB_PASS=${DB_PASS}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${DB_NAME} ]] && echo "DB_NAME=${DB_NAME}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SSH_TUNNEL_HOST} ]] && echo "SSH_TUNNEL_HOST=${SSH_TUNNEL_HOST}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SSH_TUNNEL_PORT} ]] && echo "SSH_TUNNEL_PORT=${SSH_TUNNEL_PORT}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SSH_TUNNEL_USER} ]] && echo "SSH_TUNNEL_USER=${SSH_TUNNEL_USER}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SSH_TUNNEL_PASS} ]] && echo "SSH_TUNNEL_PASS=${SSH_TUNNEL_PASS}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${PRE_SCRIPT_CMD} ]] && echo "PRE_SCRIPT_CMD=${PRE_SCRIPT_CMD}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${POST_SCRIPT_CMD} ]] && echo "POST_SCRIPT_CMD=${POST_SCRIPT_CMD}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SCRIPTING_CONNECTION} ]] && echo "SCRIPTING_CONNECTION=${SCRIPTING_CONNECTION}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SCRIPTING_HOST} ]] && echo "SCRIPTING_HOST=${SCRIPTING_HOST}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SCRIPTING_PORT} ]] && echo "SCRIPTING_PORT=${SCRIPTING_PORT}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SCRIPTING_USER} ]] && echo "SCRIPTING_USER=${SCRIPTING_USER}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${SCRIPTING_PASS} ]] && echo "SCRIPTING_PASS=${SCRIPTING_PASS}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${AWS_CONNECTION_PROFILE} ]] && echo "AWS_CONNECTION_PROFILE=${AWS_CONNECTION_PROFILE}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${AWS_REGION} ]] && echo "AWS_REGION=${AWS_REGION}" >> ${TMP_DIR}/used_config.conf ;
  [[ -n ${AWS_BUCKET} ]] && echo "AWS_BUCKET=${AWS_BUCKET}" >> ${TMP_DIR}/used_config.conf ;
  
  log_to_debug "$(cat ${TMP_DIR}/used_config.conf)"
  
  return 0
}


#######################################
# Print usage and exit.
#
# RETURN:
#   0
#######################################

function usage() {
  echo """
$(basename $0) - Creates a backup to AWS based on config file contents.

Usage:

  $(basename $0) --config-file|-c <path> [OPTIONS...]
  $(basename $0) --help
  """

  [[ x${1} == x"complete" ]] && {
    echo """
OPTIONS:
  -h|--help
    Show this help and exit

  -c|--config-file
    Configuration file for the tasks

  -l|--log-level <fatal|error|warn|info|debug>
    Sets the logging level for the application (Default: info)
  -s|--script
    Prepare output for scripting (disable colors)
  -v|-vv|-vvv
    Verbosity level
  -t|--time
    Enable profiling/time duration of different operations
  --sys-tmp-dir

  -p|--purge-tmp-dirs
    Delete all temporal folders and exit
  -m|--maintain-tmp-data
    Do not delete temporary folders on exit

  --backup-folders <paths array>
    Remote folders to backup. Comma separated array/Bash array
  --storage-host <hostname|ip address>
    Host where the backup-folders are stored
  --storage-protocol <local|sftp|ftp>
    Protocol used to connect with storage-host
  --storage-port <port number>
    Port used to connect to storage-host using storage-protocol
  --storage-user <username>
    User used to connect to storage-host using storage-protocol
  --storage-pass <password>
    Password used to connect to storage-host using storage-protocol

  --db-connection <disabled|direct|storage-tunnel|ssh-tunnel|wp-config>
    How to connecto to db-host.
      disabled: do not make database backup
      direct: the connection is made directly using the other db-* options
      storage-tunnel: A ssh tunnel using storage-* options is created before
        using db-* options to connect db-host over that tunnel (only valid for
        sftp storage-protocol).
      ssh-tunnel: A ssh tunnel using ssh-tunnel-* options is created before 
        using db-* options to connect db-host over that tunnel.
      wp-config: Looks into backup-folders at storage-host for the wp-config.php
        file, and parses it to obtain the db-* options. All db-* options are
        invalidated.
  --db-host
    Host where the database are located
  --db-protocol <mysql|postgres>
    Database type
  --db-port <port number>
    Port used to connect to db-host using db-protocol
  --db-user <username>
    User used to connect to db-host using storage-protocol
  --db-pass <password>
    Password used to connect to db-host using storage-protocol
  --db-name <database name>
    Database name


  --ssh-tunnel-host <hostname|ip address>
    Host to create the ssh-tunnel for database backup
  --ssh-tunnel-port <port number>
    Port used to connect to ssh-tunnel-host
  --ssh-tunnel-user <username>
    User used to connect to ssh-tunnel-host
  --ssh-tunnel-pass <password>
    Password used to connect to ssh-tunnel-host


  --pre-script-cmd <command>
    Command executed before backup bassed on the following scripting options
  --post-script-cmd <command>
    Command executed after backup bassed on the following scripting options

  --scripting-connection <disabled|direct|storage|ssh-tunnel>
    How to connecto to scripting-host.
      disabled: do not execute the scripts. Disable pre and post scripts.
      direct: the scripts are executed using SSH over the connection made 
        using scripting-* options.
      storage:the scripts are executed using SSH over the connection made 
        using storage-* options.
      ssh-tunnel: the scripts are executed using SSH over the connection made 
        using ssh-tunnel-* options
  --scripting-host <hostname|ip address>
    Host to direct connect for scripting
  --scripting-port <port number>
    Port used to direct connect for scripting
  --scripting-user <username>
    User used to direct connect for scripting
  --scripting-pass <password>
    Password used to direct connect for scripting

  --aws-connection-profile <awscli profile name>
    Profile to use on awscli connections
  --aws-bucket <s3 bucket name>
    AWS S3 bucket name for storing the backups
  --aws-region <aws region>
    AWS region where the aws-bucket is located
    """
  }

  return 0
}


#######################################
# Execute the needed actions to begin
# backup.
#
# VARIABLES:
#   BACKUP_NAME (mandatory)
#   DB_PROTOCOL (mandatory)
#   TMP_DIR_TEMPLATE (mandatory)
#   TMP_DIR (out)
#   OUTPUT_TAR (out)
#   STORAGE_HOST (out)
# RETURN:
#   0 if succeded, non-zero on error.
#######################################

initialize_backup() {
  log_to_debug "${FUNCNAME[0]}():"

  TMP_DIR=$(mktemp -p ${SYS_TMP_DIR} -d -t ${TMP_DIR_TEMPLATE}XXXXXXXXXX)/${BACKUP_NAME}
  OUTPUT_TAR=${TMP_DIR}_$(date +'%Y%m%d_%H%M%S').tar.gz

  [[ ${STORAGE_PROTOCOL} == local ]] && { STORAGE_HOST=$(hostname); }

  log_to_info "${FUNCNAME[0]}(): Creating temporal directory \"$(dirname ${TMP_DIR})\""
  mkdir -p ${TMP_DIR}/${STORAGE_HOST}


  if [[ ${DB_PROTOCOL} == wp-config ]]
  then
    storage_download_wp-config
    local ret_value=$?
    [[ ${ret_value} != 0 ]] && return ${ret_value}

    ddbb_read_wp-config
    ret_value=$?
    [[ ${ret_value} != 0 ]] && return ${ret_value}

    ddbb_test_connection
    ret_value=$?
    [[ ${ret_value} != 0 ]] && return ${ret_value}
  fi

  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}


#######################################
# Compress the TMP_DIR contents into
# OUTPUT_TAR file.
#
# VARIABLES:
#   OUTPUT_TAR (mandatory)
#   BACKUP_NAME (mandatory)
#   TMP_DIR (mandatory)
# RETURN:
#   0 if succeded, non-zero on error.
#######################################

pack_backup_contents(){
  log_to_debug "${FUNCNAME[0]}():"

  tar cfvz ${OUTPUT_TAR} -C $(dirname ${TMP_DIR}) ${BACKUP_NAME} > /dev/null
  local ret_value=$?
  [[ ${ret_value} != 0 ]] &&
    {
      log_to_error "${FUNCNAME[0]}(): tar command exited with non zero status"
      log_to_error "${FUNCNAME[0]}(): Check ${OUTPUT_TAR} output file"
      return ${ret_value}
    }

  log_to_info "${FUNCNAME[0]}(): Created output file: ${OUTPUT_TAR}: OK"
  return 0
}


#######################################
# Delete TMP_DIR contents from the
# system, unless MAINTAIN_TMP_DATA
# is set to 1.
#
# VARIABLES:
#   OUTPUT_TAR (mandatory)
#   BACKUP_NAME (mandatory)
#   TMP_DIR (mandatory)
# RETURN:
#   0 if succeded, non-zero on error.
#######################################

delete_temp_folder(){
  log_to_debug "${FUNCNAME[0]}():"

  [[ ${MAINTAIN_TMP_DATA} == 1 ]] &&
    {
      log_to_warn "${FUNCNAME[0]}(): Not deleting temporal data [$(dirname ${TMP_DIR})].";
      log_to_warn "${FUNCNAME[0]}(): Use '${0} --purge-tmp-dirs' to delete it (and all other existing ones).";
      return 0;
    }

  [[ -n ${TMP_DIR} ]] && [[ -e $(dirname ${TMP_DIR}) ]] && { rm -rf $(dirname ${TMP_DIR}) ; }

  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}


#######################################
# Delete all temporal directories
# based on SYS_TMP_DIR and
# TMP_DIR_TEMPLATE variables, unless
# MAINTAIN_TMP_DATA is not set to 1.
#
# VARIABLES:
#   OUTPUT_TAR (mandatory)
#   BACKUP_NAME (mandatory)
#   TMP_DIR (mandatory)
# RETURN:
#   0 if succeded, non-zero on error.
#######################################

purge_temp_folders(){
  log_to_debug "${FUNCNAME[0]}():"

  [[ -e ${SYS_TMP_DIR} ]] && {
    for cur_folder in $(ls -d ${SYS_TMP_DIR}/${TMP_DIR_TEMPLATE}* 2> /dev/null)
    do
      log_to_warn "${FUNCNAME[0]}(): Deleting ${cur_folder}"
      rm -rf ${cur_folder}
      local ret_value=$?
      [[ ${ret_value} != 0 ]] && return ${ret_value}
    done
  }

  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}
