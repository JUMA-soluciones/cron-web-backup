#/bin/bash

# Bash script to easily backup folders and database from local or remote 
# FTP or SFTP locations to a bucket in AWS S3.
#
# Copyright 2021 MarMarAba <mario@marmaraba.com>
#
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#######################################
# Calls the apropiate shell test 
# connection function based on 
# SCRIPTING_CONNECTION variable.
#
# Interface transactional function.
# 
# VARIABLES:
#   SCRIPTING_CONNECTION (mandatory)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

function shell_test_connection() {
  log_to_debug "${FUNCNAME[0]}():"
  
  shell_test_connection_${SCRIPTING_CONNECTION}
  ret_value=$?
  [[ ${ret_value} != 0 ]] && { log_to_error "${FUNCNAME[0]}(): Testing shell connection over ${SCRIPTING_CONNECTION}"; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}


#######################################
# Dummy debugging function.
#
# RETURN:
#   Zero.
#######################################

shell_test_connection_(){
  log_to_debug "${FUNCNAME[0]}():"
  return 0
}


#######################################
# Dummy debugging function.
#
# RETURN:
#   Zero.
#######################################

shell_test_connection_disabled(){
  log_to_debug "${FUNCNAME[0]}():"
  return 0
}


#######################################
# Test the connection to host 
# using storage-tunnel configuration
# function.
#
# VARIABLES:
#   STORAGE_HOST (mandatory)
#   STORAGE_PORT (mandatory)
#   STORAGE_USER (mandatory)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_test_connection_storage-tunnel(){
  log_to_debug "${FUNCNAME[0]}():"
  
  shell_test_connection_shell ${STORAGE_HOST} ${STORAGE_PORT} ${STORAGE_USER}
  local ret_value=$?
  [[ ${ret_value} != 0 ]] && { log_to_error "${FUNCNAME[0]}(): Unable to test connection shell"; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}


#######################################
# Test the connection to host 
# using STORAGE_* configuration
# function.
#
# VARIABLES:
#   STORAGE_HOST (mandatory)
#   STORAGE_PORT (mandatory)
#   STORAGE_USER (mandatory)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_test_connection_storage(){
  log_to_debug "${FUNCNAME[0]}():"
  
  shell_test_connection_direct ${STORAGE_HOST} ${STORAGE_PORT} ${STORAGE_USER}
  local ret_value=$?
  [[ ${ret_value} != 0 ]] && { log_to_error "${FUNCNAME[0]}(): Unable to test shell connection over storage connection"; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  
  return 0
}


#######################################
# Test the connection to host 
# using ssh-tunnel configuration
# function.
#
# VARIABLES:
#   SSH_TUNNEL_HOST (mandatory)
#   SSH_TUNNEL_PORT (mandatory)
#   SSH_TUNNEL_USER (mandatory)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_test_connection_ssh-tunnel(){
  log_to_debug "${FUNCNAME[0]}():"
  
  shell_test_connection_direct ${SSH_TUNNEL_HOST} ${SSH_TUNNEL_PORT} ${SSH_TUNNEL_USER}
  local ret_value=$?
  [[ ${ret_value} != 0 ]] && { log_to_error "${FUNCNAME[0]}(): Unable to test shell connection over ssh-tunnel tunnel"; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}


#######################################
# Test the connection to host 
# using scripting configuration
# function.
#
# VARIABLES:
#   SCRIPTING_HOST
#   SCRIPTING_PORT
#   SCRIPTING_USER
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_test_connection_direct(){
  log_to_debug "${FUNCNAME[0]}():"
  
  local m_ssh_host=${SCRIPTING_HOST}
  local m_ssh_port=${SCRIPTING_PORT}
  local m_ssh_user=${SCRIPTING_USER}
  local m_ssh_pass=${SCRIPTING_PASS}
  
  [[ -n ${1} ]] && { m_ssh_host=${1}; }
  [[ -n ${2} ]] && { m_ssh_port=${2}; }
  [[ -n ${3} ]] && { m_ssh_user=${3}; }
  [[ -n ${4} ]] && { m_ssh_pass=${4}; }
  
  ssh -o StrictHostKeyChecking=no -p ${m_ssh_port} ${m_ssh_user}@${m_ssh_host} true
  local ret_value=$?
  
  [[ "${ret_value}" != "0" ]] && { log_to_error "${FUNCNAME[0]}(): Unable to test scripting connection"; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  
  return 0
}


#######################################
# Check if pre-script is needed
# to be executed, and executes it.
#
# VARIABLES:
#   PRE_SCRIPT_CMD (optional)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

function shell_exec_pre_script() {
  log_to_debug "${FUNCNAME[0]}():"
  
  [[ -z ${PRE_SCRIPT_CMD} ]] && { return 0; }
  
  shell_execute_script_${SCRIPTING_CONNECTION} "${PRE_SCRIPT_CMD}"
  ret_value=$?
  [[ ${ret_value} != 0 ]] && { log_to_error "${FUNCNAME[0]}(): Unable to execute pre-script using ${SCRIPTING_CONNECTION} connection."; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  
  return 0
}

#######################################
# Check if post-script is needed
# to be executed, and executes it.
#
# VARIABLES:
#   PRE_SCRIPT_CMD (optional)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

function shell_exec_post_script() {
  log_to_debug "${FUNCNAME[0]}():"
  
  [[ -z ${POST_SCRIPT_CMD} ]] && { return 0; }
  
  shell_execute_script_${SCRIPTING_CONNECTION} "${POST_SCRIPT_CMD}"
  ret_value=$?
  [[ ${ret_value} != 0 ]] && { log_to_error "${FUNCNAME[0]}(): Unable to execute pre-script using ${SCRIPTING_CONNECTION} connection."; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  return 0
}




#######################################
# Do nothing when the SCRIPTING is 
# disabled.
#
# RETURN:
#   Returns the value allways zero.
#######################################

shell_execute_script_disabled(){
  log_to_debug "${FUNCNAME[0]}():"
  return 0
}


#######################################
# Executes de SCRIPTING_CMD command 
# using the SCRIPTING_* variables.
#
# VARIABLES:
#   SCRIPTING_CMD
#   SCRIPTING_HOST
#   SCRIPTING_PORT
#   SCRIPTING_USER
#   SCRIPTING_PASS
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_execute_script_direct(){
  log_to_debug "${FUNCNAME[0]}(): ${1}"
  
  local m_ssh_cmd=${1}
  local m_ssh_host=${SCRIPTING_HOST}
  local m_ssh_port=${SCRIPTING_PORT}
  local m_ssh_user=${SCRIPTING_USER}
  local m_ssh_pass=${SCRIPTING_PASS}
  
  [[ -n ${2} ]] && { m_ssh_host=${2}; }
  [[ -n ${3} ]] && { m_ssh_port=${3}; }
  [[ -n ${4} ]] && { m_ssh_user=${4}; }
  [[ -n ${5} ]] && { m_ssh_pass=${5}; }

  m_redir="" && [[ "${LOG_LEVEL}" != "DEBUG" ]] &&  m_redir="2>&1 > /dev/null" 

  ssh -o StrictHostKeyChecking=no -p ${m_ssh_port} ${m_ssh_user}@${m_ssh_host} "${1}" ${m_redir}

  local ret_value=$?
  [[ "${ret_value}" != "0" ]] && { log_to_error "${FUNCNAME[0]}(): Unable execute script '${1}'."; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  
  return 0
}

#######################################
# Executes de SCRIPTING_CMD command 
# using the STORAGE_* variables.
#
# VARIABLES:
#   STORAGE_HOST (mandatory)
#   STORAGE_PORT (mandatory)
#   STORAGE_USER (mandatory)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_execute_script_storage(){
  log_to_debug "${FUNCNAME[0]}():"
  
  shell_execute_script_direct "${1}" ${STORAGE_HOST} ${STORAGE_PORT} ${STORAGE_USER} ${STORAGE_PASS}
  local ret_value=$?
  [[ "${ret_value}" != "0" ]] && { log_to_error "${FUNCNAME[0]}(): Unable execute script."; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  
  return 0
}

#######################################
# Executes de SCRIPTING_CMD command 
# using the STORAGE_* variables.
#
# VARIABLES:
#   SSH_TUNNEL_HOST (mandatory)
#   SSH_TUNNEL_PORT (mandatory)
#   SSH_TUNNEL_USER (mandatory)
#   SSH_TUNNEL_PASS (optional)
# RETURN:
#   Returns the value returned by the 
#   underlaying function.
#######################################

shell_execute_script_ssh-tunnel(){
  log_to_debug "${FUNCNAME[0]}():"
  
  shell_execute_script_direct "${1}" ${SSH_TUNNEL_HOST} ${SSH_TUNNEL_PORT} ${SSH_TUNNEL_USER} ${SSH_TUNNEL_PASS}
  local ret_value=$?
  [[ "${ret_value}" != "0" ]] && { log_to_error "${FUNCNAME[0]}(): Unable execute script."; return ${ret_value}; }
  
  log_to_info "${FUNCNAME[0]}(): OK"
  
  return 0
}
