#!/bin/bash

PARALLEL_THREADS=5
BJACKUP_OPTIONS=""
CONFIG_DIR="/etc/bjackup"
FAILED_BACKUPS_FILE=$(mktemp)


while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
    -r|--PARALLEL_THREADS)
      PARALLEL_THREADS=${2}
      shift 2
    ;;
    -d|--config-dir)
      CONFIG_DIR=${2}
      shift 2
    ;;
    # Guardamos el resto de opciones en $BJACKUP_OPTIONS
    -h|--help|-p|--purge-tmp-dirs|-m|--maintain-tmp-data|-s|--script-run|-v|-vv|-vvv|-t|--time)
      BJACKUP_OPTIONS="${BJACKUP_OPTIONS} ${1}"
      shift 1
    ;;
    *)
      BJACKUP_OPTIONS="${BJACKUP_OPTIONS} ${1} ${2}"
      shift 2
    ;;
  esac
done


# Para cada fichero en $CONFIG_DIR lanzar una tarea

for file in $( ls ${CONFIG_DIR}/*.conf ) ; do

  # Ejecutar las tareas en segundo plano, usando las $BJACKUP_OPTIONS y el fichero $file acutal
  ( /bin/bash -l /srv/backup/scripts/bjackup.sh -c $file ${BJACKUP_OPTIONS} || ( echo "Error ejecutando backup de $file" && ( echo ${file} >> ${FAILED_BACKUPS_FILE}) ) ) &

  sleep 10

  # Permitir ejecutar $PARALLEL_THREADS trabajos en paralelo
  if [[ $(jobs -r -p | wc -l) -gt $PARALLEL_THREADS ]]; then
      # Si llegamos aquí ya hay $PARALLEL_THREADS ejecutando
      # así que esperamos a que finalice uno para lanzar el siguiente.
      wait -n
  fi
done

# Esperamos a que terminen todos los procesos
wait

if [[ -s ${FAILED_BACKUPS_FILE} ]]
then
  echo "--- Contenido del fichero de errores ---"
  cat ${FAILED_BACKUPS_FILE}
fi

rm ${FAILED_BACKUPS_FILE}
